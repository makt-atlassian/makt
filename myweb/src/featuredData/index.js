import featureInfo from "./feature";

const featuredData = [...featureInfo];

const shuffle = (a) => {
  let temp;

  for (let i = a.length; i; i--) {
    let j = Math.floor(Math.random() * i);
    temp = a[i - 1];
    a[i - 1] = a[j];
    a[j] = temp;
  }
};

shuffle(featuredData);

export default featuredData;
