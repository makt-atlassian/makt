const featureInfo = [
  {
    id: 1,
    title: "Certified Ethical Hacker",
    subtitle: "Powerd By Amazon",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit modi dolorem quis quae animi nihil minus sed unde voluptas cumque",
    url: "https://i.ibb.co/kqSDMFR/emp18.png",
  },

  {
    id: 2,
    title: "Certified Ethical Hacker",
    subtitle: "Powerd By Amazon",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit modi dolorem quis quae animi nihil minus sed unde voluptas cumque",
    url: "https://i.ibb.co/72stdCn/emp2.jpg",
  },

  {
    id: 3,
    title: "Certified Ethical Hacker",
    subtitle: "Powerd By Amazon",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit modi dolorem quis quae animi nihil minus sed unde voluptas cumque",
    url: "https://i.ibb.co/tZ8JJ9M/emp1.jpg",
  },

  {
    id: 4,
    title: "Certified Ethical Hacker",
    subtitle: "Powerd By Amazon",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit modi dolorem quis quae animi nihil minus sed unde voluptas cumque",
    url: "https://i.ibb.co/hcF0Kdr/emp3.jpg",
  },

  {
    id: 5,
    title: "Certified Ethical Hacker",
    subtitle: "Powerd By Amazon",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit modi dolorem quis quae animi nihil minus sed unde voluptas cumque",
    url: "https://i.ibb.co/5rjS7tV/emp12.jpg",
  },

  {
    id: 6,
    title: "Certified Ethical Hacker",
    subtitle: "Powerd By Amazon",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit modi dolorem quis quae animi nihil minus sed unde voluptas cumque",
    url: "https://i.ibb.co/dbp3DNp/emp8.jpg",
  },

  {
    id: 7,
    title: "Certified Ethical Hacker",
    subtitle: "Powerd By Amazon",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit modi dolorem quis quae animi nihil minus sed unde voluptas cumque",
    url: "https://i.ibb.co/9tR3cW7/emp10.png",
  },

  {
    id: 8,
    title: "Certified Ethical Hacker",
    subtitle: "Powerd By Amazon",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit modi dolorem quis quae animi nihil minus sed unde voluptas cumque",
    url: "https://i.ibb.co/1MdFBCh/emp6.jpg",
  },
];

export default featureInfo;
