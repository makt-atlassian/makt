import React, { useState } from "react";
import Featured from "./Featured";
import Nav from "./nav";
import featuredData from "../../featuredData";
import "./Featured.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import $ from "jquery";
import jQuery from "jquery";
import "react-slick";

const NewRelease = () => {
  // state = {
  //   data: [],
  // };

  // const getData = async () => {
  //   const res = await fetch({ featuredData });
  //   const data = await res.json();
  //   this.setState({ data: data });
  // };

  // componentDidMount = () => {
  //   this.getData();
  // };

  const data = featuredData.slice(0, 8);
  const [featured, setFeatured] = useState(data);

  const myRef = React.createRef();

  const prevClick = () => {
    const slide = myRef.current;
    slide.scrollLeft -= slide.offsetWidth;
    if (slide.scrollLeft <= 0) {
      slide.scrollLeft = slide.scrollWidth;
    }
  };

  const nextClick = () => {
    const slide = myRef.current;
    slide.scrollLeft += slide.offsetWidth;
    if (slide.scrollLeft >= slide.scrollWidth - slide.offsetWidth) {
      slide.scrollLeft = 1;
    }
  };

  return (
    <div>
      <h2>New Release</h2>
      <div className="newRelease">
        <div className="wrapper">
          <div className="app">
            {featured.map((ps) => (
              <Featured featured={ps} ref={myRef} />
            ))}
          </div>

          <Nav prev={prevClick} next={nextClick} />
        </div>
      </div>
    </div>
  );
};

jQuery(document).ready(function () {
  $(".newRelease").slick({
    arrows: true,
    autoplay: true,
    dots: true,
    swipe: true,
  });
});

export default NewRelease;
