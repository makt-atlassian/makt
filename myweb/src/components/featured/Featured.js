import React from "react";
// import "./Featured.css";

const Featured = (props) => {
  const { url, title, subtitle, description } = props.featured;

  return (
    <div className="card-portion">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="container">
              <div class="row">
                <div class="col-sm-12 col-lg-3">
                  <div
                    class="card text-center"
                    style={{ width: "300px", margin: "auto" }}
                  >
                    <img src={url} class="card-img-top" />
                    <div class="card-body">
                      <h4 class="card-title">{title}</h4>
                      <h5>{subtitle}</h5>
                      <p class="card-text">{description}</p>
                      <button type="button" class="btn btn-dark p-2 m-2">
                        Read More
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Featured;
