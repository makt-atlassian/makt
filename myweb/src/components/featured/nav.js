import React from "react";
import prev1 from "../../images/New_Release_Item/prev.png";
import next1 from "../../images/New_Release_Item/next.png";

class Nav extends React.Component {
  render() {
    const { next, prev } = this.props;
    return (
      <div className="row">
        <div className="prev" onClick={prev}>
          <img src={prev1} alt="" />
        </div>
        <div className="next" onClick={next}>
          <img src={next1} alt="" />
        </div>
      </div>
    );
  }
}

export default Nav;
